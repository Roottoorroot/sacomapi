class Config

  # Get STB host for request
  def get_stb_host ()
    return 'https://cardtest.sacombank.com.vn'
  end

  # Get STB port for request
  def get_stb_port
    return '9443'
  end

  # Get STB username for request
  def get_stb_username ()
    return '1a58a26b-d200-47ce-bfa2-397ca15cf0f7'
  end

  # Get STB password for request
  def get_stb_password ()
    return '9c42e6c069b3de62'
  end

  # Get secret key path for encrypt
  def get_stb_key_path ()
    return 'RSA/testRsa.pem'
  end

  # Get pending resp code
  def get_pending_resp_code ()
    return ['01','05','68','91','94']
  end
end

class Security

  require 'digest'
  require 'openssl'
  require 'base64'
  require 'json'

  # Encrypt in md5 hex
  def md5_hex(input)
    md5 = Digest::MD5.new
    md5.update input.to_s
    result = md5.hexdigest
    return result.upcase
  end

  # Encrypt in 3DES, will need a secret key
  def encrypt_3des (input_string, key)

    cipher = OpenSSL::Cipher::Cipher.new('DES-EDE3-CBC')
    cipher.pkcs5_keyivgen(key, nil)
    cipher.encrypt
    text_encrypted = cipher.update(input_string.to_s) + cipher.final

    return Base64.encode64(text_encrypted)
  end

  # Create a signature with RSA private key
  def sign(input)

    conf = Config.new
    key_path = conf.get_stb_key_path

    pre_sign = md5_hex(input)
    private_key = OpenSSL::PKey::RSA.new(File.read(key_path))
    signature = private_key.sign(OpenSSL::Digest::SHA256.new, pre_sign)

    return signature
  end

end


class Stb_api_int

  require 'json'
  require 'securerandom'
  require 'net/http'

  def verify_card (params)

    params.fetch('IsAuthentication', true)
    params.fetch('IsEnrollment', true)
    params.fetch('IsValidation', false)

     return get_response(params, 'EcomAuthorization')
  end

  def verify_otp (params)

    params.fetch('IsAuthentication',  true)
    params.fetch('IsEnrollment', false)
    params.fetch('IsValidation', true)

    return get_response(params, 'EcomAuthorization')
  end

  def query (params)

    params.fetch('Type', 'EcomAuthorization')

    return get_response(params, 'EcomAuthorization')
  end

  def check_balanse

    current_time = Time.now.utc.iso8601
    request_data_time = current_time.to_s
    params = {'RefNumber' => 'WC' + request_data_time}

    return get_response(params, '"EcomMerchantBalanceInquiry"')
  end

  def get_response (params, fnc)

    security = Security.new
    config = Config.new
    current_time = Time.now.utc.iso8601
    request_data_time = current_time.to_s



    data = security.encrypt_3des(params, config.get_stb_password)
    function_name = fnc
    request_id = get_uuid

    request = {"Data" => data , "FunctionName" => function_name,
               "RequestDataTime" => request_data_time, "RequestID" => request_id}.to_json

    return call(request)
  end

  def call (request)

    conf = Config.new
    security = Security.new

    signature = security.sign(request)

    @post_ws = URI("https://cardtest.sacombank.com.vn:944/e3pay/stb/")
    @init_header ={
        'Content-Type' =>'Application/json',
        'Accept' => 'application/json',
        'Signature' => signature
    }.to_json

    req = Net::HTTP::Post.new(@post_ws, initheader = @init_header)
    req.basic_auth conf.get_stb_username, conf.get_stb_password
    req.body = request
    response = Net::HTTP.new(conf.get_stb_host, conf.get_stb_port).start {|http| http.request(req) }
    return "Response #{response.code} #{response.message}:
          #{response.body}"

  end

  def get_uuid

    uuid = SecureRandom.uuid
    return uuid
  end

end
