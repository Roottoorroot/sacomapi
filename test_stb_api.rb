class Test_stb_API
require_relative 'stb_api_int'
require 'time'

  stb_api = Stb_api_int.new
  config = Config.new
  current_time = Time.now.utc.iso8601
  request_data_time = current_time.to_s


  verify_card = {
                'CardNumber' => '9704030003772081',
                'Amount' => 1,
                'FullName' => 'NGUYEN SY NGUYEN',
                'SSN' => '012345678',
                'Description' => 'Test',
                'RefNumber' => 'WC' + request_data_time
                }

  verify_card_result = stb_api.verify_card(verify_card)

  if verify_card_result['RespCode'] == '10'
    # Require to verify OTP
    # Go to step 2
    puts 'Ok second step'
  else
   puts "Failed" # Failed -> show resp code description
  end

  # Step two

  verify_otp_params = {
                'OTP' => '123456', # Otp or token sent to customer (via mobile or token device)
                'Amount' => 10000, # Order's total amount (VND)
                'RefNumber' => 'xxx' # is RefNumber of the above verifyCard message
                }

  verify_otp_result = stb_api.verify_otp(verify_otp_params)
  resp_code = verify_otp_result.fetch('RespCode')

  if resp_code == '00'
    puts 'Successful'
  elsif resp_code.include?(config.get_pending_resp_code)
    # Time out, pending , cannot get response, no result -> set transaction's status = pending/timeout
    # Call API query later to check result
      puts "Status: pending/timeout"
    else
      puts "Failed: show resp code description"
  end

  # Query if cannot get final result

  query_params = {
              'Date' => Date.new('Ymd'), # transaction's DATE
              'RefNumber' => 'xxx' # transaction's refNumber that need to check
  }

  query_Result = stb_api.query(query_params)
  resp_code = query_Result.fetch('RespCode')

  if resp_code == '00'
    puts "Success"
  elsif
    puts "Not answer -> wait to reconcile"
  else
    puts "Failed"
  end
end